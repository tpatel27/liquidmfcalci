### Introduction

This is a [React Js](https://reactjs.org/) Application called Liquid MF Calculator which calculates the units using
Net Asset Values and the given amount by using this formula: (Units = Amount / NAV)

### Assumptions

-  Possibility of upload file action which can take any local file as well

### Installation

- Take a clone or download the zip

  #### `npm install`

- Install the dependencies in the local node_modules folder.

  #### `npm start`

- To run the application







