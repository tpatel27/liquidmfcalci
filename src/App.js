import React, { Component } from 'react';
import './App.css';
import { connect } from 'react-redux';
import TextField from '@material-ui/core/TextField';
import { bindActionCreators } from 'redux';
import * as actionCreators from './action/index';
import { colors } from '@material-ui/core';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      nameDisabled: true,
      btnDisabled: true,
  }
}

showFile = async (e) => {
  e.preventDefault()
  const reader = new FileReader()
  reader.onload = async (e) => { 
    const text = (e.target.result)
    this.props.file(text);
    var cells = text.split('\n').map(function (el) { return el.split(/\;+/); });
    console.log("cells", cells)
    console.log("cells", cells.map(item => item[1]+ ' ' +item[4]))
    var headings = cells.shift();
    var obj = cells.map(function (el) {
      // var obj = {};
      for (var i = 0, l = el.length; i < l; i++) {
        // obj[headings[i]] = isNaN(Number(el[i])) ? el[i] : +el[i];
      }
      return obj;
    });
    console.log(text)
  };
  reader.readAsText(e.target.files[0])
}

render(){
  return(
    <div>
      <div className="App">
          <div className="App-header">
            Calculator for MF
          </div>
      </div>
      <div className="calculator" style={{textAlign: 'center'}}>
        <input type="file" id="fileInput" onChange={(e) => this.showFile(e)} />
          <div>
            <div>
                <TextField
                  required
                  id="standard-full-width"
                  label="Amount"
                  placeholder="10000"
                  margin="normal"
                  InputLabelProps={{
                  fontSize: '10px'
                  }}
                  onChange={(text) => this.setState({nameDisabled:!text.target.value})}
                  disabled={!this.state.btnDisabled}
                />
              </div>
              <div>
                <TextField
                  required
                  id="standard-full-width"
                  label="Units"
                  placeholder="10000"
                  margin="normal"
                  InputLabelProps={{
                  fontSize: '10px'
                  }}
                  onChange={(text) => this.setState({btnDisabled:!text.target.value})}
                  disabled={!this.state.nameDisabled}
                />
            </div>
          </div>
      </div>
    </div>
  )
}
}

function mapStateToProps(state) {  
  return {
     files: state.files
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actionCreators, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);



