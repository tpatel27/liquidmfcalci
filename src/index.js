import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import App from './App.js'
import counter from './reducers/counter'
import './index.css';

const store = createStore(counter);
render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);